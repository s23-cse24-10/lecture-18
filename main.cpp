#include <iostream>
#include "board.h"
using namespace std;

int main() {

    Board board;
    board.grid[0][0] = PLAYER1;
    board.grid[1][1] = PLAYER2;

    Board backup = board;

    board.grid[2][2] = PLAYER2;

    cout << "board: " << (long) board.grid << endl;
    cout << board;

    cout << "backup: " << (long) backup.grid << endl;
    cout << backup;

    return 0;
}